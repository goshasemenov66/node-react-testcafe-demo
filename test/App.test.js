import { Selector } from 'testcafe';
import { waitForReact } from 'testcafe-react-selectors';

fixture `App tests`
    .page('http://localhost:8080')
    .beforeEach(async () => {
        await waitForReact();
    });

test('Main page has logo', async t => {
    const logo = Selector('.App-logo');
    await t
        .expect(logo.exists).ok('Expected an element with class App-logo');
});